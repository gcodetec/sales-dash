# Sales Dashboard

#### Preparando ambiente

* `docker-compose build`
* `docker-compose up -d`
  * postgres
  * mongo
  * api java
  * frontend angular

#### Rodando os testes

* API SalesDash - `cd salesdashapi && ./scripts/tests`
* Frontend - `cd spa && ./scripts/tests`

#### Deploy

* API SalesDash
  * TODO
* Frontend
  * TODO

