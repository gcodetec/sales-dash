import { CustoService } from './../../custos/shared/custo.service';
import { Injector } from '@angular/core';
import { Pessoa } from './../../pessoas/shared/pessoa.model';
import { Obra } from './../shared/obra.model';
import { Component, OnInit } from '@angular/core';
import { ObraService } from '../shared/obra.service';
import { Validators } from '@angular/forms';
import { Custo } from '../../custos/shared/custo.model';
import { PessoaService } from '../../pessoas/shared/pessoa.service';
import { BaseResourceForm } from 'src/app/shared/base-resource-form/base-resource-form.abstract';


@Component({
  selector: 'app-obra-form',
  templateUrl: './obra-form.component.html',
  styleUrls: ['./obra-form.component.css'],

})

export class ObraFormComponent extends BaseResourceForm<Obra> {

  constructor(
    protected obraService: ObraService,
    protected custoService: CustoService,
    protected pessoaService: PessoaService,
    protected injector: Injector) {
    super(injector, new Obra(), obraService, Obra.fromJson);
    this.loadCustos();
    this.loadEmpreiteiros();
    this.loadClientes();
    this.loadEngenheiros();
    this.loadGerenteContratos();
    this.loadAdministrativos();
    this.loadCompradores();

  }
  empreiteiros: Array<Pessoa>;
  engenheiros: Array<Pessoa>;
  gerenteContratos: Array<Pessoa>;
  compradores: Array<Pessoa>;
  clientes: Array<Pessoa>;
  administrativos: Array<Pessoa>;
  custos: Array<Custo>;
  compare = 'id';
  ptBR = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
    monthNames: [
      'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
      'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
    ],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    today: 'Hoje',
    clear: 'Limpar'
  };

 compareFn = this._compareFn.bind(this);

  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      id: [null],
      descricao: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(150) ]],
      endereco: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(250) ]],
      dataInc: [null, Validators.required],
      dataFim: [null],
      empreiteiro: [null, Validators.required],
      comprador: [null],
      administrativo: [null],
      engenheiro: [null],
      gerenteContrato: [null],
      cliente: [null],
      custo: [null],
      ativo: []


    });
  }

 _compareFn(a, b) {
   if (a && b ) {
     return a[this.compare] === b[this.compare];
   }
 }


  protected creationPageTitle(): string {

    return 'Cadastro de Obras';
  }

  protected editionPageTitle(): string {
     return 'Editando: ';
  }


  private loadCustos() {

    this.custoService.getAll().subscribe(
      custos => this.custos = custos
    );
  }

  private loadEmpreiteiros() {
    this.pessoaService.pessoas(`/empreiteiros`).subscribe(
      empreiteiros => this.empreiteiros = empreiteiros
    );
  }

  private loadGerenteContratos() {
    this.pessoaService.pessoas(`/gerenteContratos`).subscribe(
      gerenteContratos => this.gerenteContratos = gerenteContratos
    );
  }

  private loadEngenheiros() {
    this.pessoaService.pessoas(`/engenheiros`).subscribe(
      engenheiros => this.engenheiros = engenheiros
    );
  }

  private loadClientes() {
    this.pessoaService.pessoas(`/clientes`).subscribe(
      clientes => this.clientes = clientes
    );
  }

  private loadAdministrativos() {
    this.pessoaService.pessoas(`/administrativos`).subscribe(
      administrativos => this.administrativos = administrativos
    );
  }

  private loadCompradores() {
    this.pessoaService.pessoas(`/compradores`).subscribe(
      compradores => this.compradores = compradores
    );
  }
}
