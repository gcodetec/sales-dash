import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ObrasRoutingModule } from './obras-routing.module';
import { ObraListComponent } from './obra-list/obra-list.component';
import { ObraFormComponent } from './obra-form/obra-form.component';

@NgModule({
  imports: [
    SharedModule,
    ObrasRoutingModule
  ],
  declarations: [ObraListComponent, ObraFormComponent]
})
export class ObrasModule { }
