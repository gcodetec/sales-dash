import { Component, OnInit } from '@angular/core';
import { BaseResourceList } from 'src/app/shared/base-resource-list/base-resource-list.abstract';
import { Obra } from '../shared/obra.model';
import { ObraService } from '../shared/obra.service';

@Component({
  selector: 'app-obra-list',
  templateUrl: './obra-list.component.html',
  styleUrls: ['./obra-list.component.css']
})
export class ObraListComponent extends BaseResourceList<Obra> {

  constructor(private obraService: ObraService) {
    super(obraService);
  }

}
