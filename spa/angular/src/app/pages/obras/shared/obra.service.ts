import { Obra } from './obra.model';

import { NgModule, Injectable, Injector } from '@angular/core';
import { BaseResourceService } from 'src/app/shared/services/base-resource.service';

@Injectable({
  providedIn: 'root'
})
export class ObraService extends BaseResourceService<Obra> {


  constructor(protected injector: Injector) {

    super("http://localhost:9090/obras", injector, Obra.fromJson);

  }
}
