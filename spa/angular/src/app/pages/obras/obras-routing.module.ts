import { ObraFormComponent } from './obra-form/obra-form.component';
import { ObraListComponent } from './obra-list/obra-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: ObraListComponent},
  { path: 'new', component: ObraFormComponent},
  { path: ':id/edit', component: ObraFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObrasRoutingModule { }
