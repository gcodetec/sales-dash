import { map } from 'rxjs/operators';
import { Injectable, Injector } from '@angular/core';


import { BaseResourceService } from "../../../shared/services/base-resource.service";
import { Pessoa } from './pessoa.model';
import { CargoService } from '../../cargos/shared/cargo.service';
import { Observable } from 'rxjs';
import { flatMap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PessoaService extends BaseResourceService<Pessoa> {


  constructor(protected injector: Injector, private cargoService: CargoService) {

    super("http://localhost:9090/pessoas", injector, Pessoa.fromJson);

  }

  private setCargo(pessoa: Pessoa, sendFn: any): Observable<Pessoa>{
    return this.cargoService.getById(pessoa.cargo.id).pipe(
      flatMap(cargo => {
        pessoa.cargo = cargo;
        return sendFn(pessoa)
      }),
      catchError(this.handleError)
    );
  }
  pessoas(tipo: string): Observable<Pessoa[]> {
    return this.http.get(this.apiPath.concat(tipo)).pipe(
      map(this.jsonDataToResources.bind(this)),
      catchError(this.handleError)
    )

  }


}
