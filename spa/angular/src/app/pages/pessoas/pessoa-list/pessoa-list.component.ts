import { Component, OnInit } from '@angular/core';
import { BaseResourceList } from 'src/app/shared/base-resource-list/base-resource-list.abstract';
import { Pessoa } from '../shared/pessoa.model';
import { PessoaService } from '../shared/pessoa.service';


@Component({
  selector: 'app-pessoa-list',
  templateUrl: './pessoa-list.component.html',
  styleUrls: ['./pessoa-list.component.css']
})
export class PessoaListComponent extends BaseResourceList<Pessoa> {

  constructor(private pessoaService: PessoaService) {
    super(pessoaService);
  }
}
