import { Injector } from '@angular/core';
import { Custo } from './../shared/custo.model';
import { Component, OnInit } from '@angular/core';
import { CustoService } from '../shared/custo.service';
import { Validators } from '@angular/forms';
import { BaseResourceForm } from 'src/app/shared/base-resource-form/base-resource-form.abstract';

@Component({
  selector: 'app-custo-form',
  templateUrl: './custo-form.component.html',
  styleUrls: ['./custo-form.component.css']
})
export class CustoFormComponent extends BaseResourceForm<Custo> {

  constructor(protected custoService: CustoService, protected injector: Injector) {
    super(injector, new Custo(), custoService, Custo.fromJson);
  }


  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      id: [null],
      descricao: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(150) ]],
      ativo: [true, Validators.required]
    });
  }


  protected creationPageTitle(): string {
    return 'Cadastro de Custo';
  }

  protected editionPageTitle(): string {
    const custoName = this.resource.descricao || '';
    return 'Editando: ';
  }
}
