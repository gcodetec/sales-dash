import { CustoFormComponent } from './custo-form/custo-form.component';
import { CustoListComponent } from './custo-list/custo-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: CustoListComponent},
  { path: 'new', component: CustoFormComponent},
  { path: ':id/edit', component: CustoFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustosRoutingModule { }
