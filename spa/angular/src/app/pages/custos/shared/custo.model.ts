import { BaseResourceModel } from "../../../shared/models/base-resource.model";

export class Custo extends BaseResourceModel {
  constructor(
    public id?:number,
    public descricao?: string,
    public ativo?: boolean
  ){
    super();
  }


  static fromJson(jsonData: any): Custo {
    return Object.assign(new Custo(), jsonData);
  }
}
