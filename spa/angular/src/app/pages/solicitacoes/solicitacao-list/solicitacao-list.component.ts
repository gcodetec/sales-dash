import { SolicitacaoService } from './../shared/solicitacao.service';
import { Solicitacao } from './../shared/solicitacao.model';
import { Component, OnInit } from '@angular/core';
import { BaseResourceList } from 'src/app/shared/base-resource-list/base-resource-list.abstract';

@Component({
  selector: 'app-solicitacao-list',
  templateUrl: './solicitacao-list.component.html',
  styleUrls: ['./solicitacao-list.component.css']
})
export class SolicitacaoListComponent extends BaseResourceList<Solicitacao> {

  constructor(private solicitacaoService: SolicitacaoService) {
    super(solicitacaoService);
  }

}
