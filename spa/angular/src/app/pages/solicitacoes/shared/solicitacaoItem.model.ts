import { BaseResourceModel } from 'src/app/shared/models/base-resource.model';
export class SolicitacaoItem extends BaseResourceModel {
  constructor(
    public id?:number,
    public descricao?:string,
    public observacao?:string,
    public statusItem?:string,
    public tipoItem?:string,
    public qtd?:string,
    public ativo?:boolean


  ){
    super();
  }


  static fromJson(jsonData: any): SolicitacaoItem {
    return Object.assign(new SolicitacaoItem(), jsonData);
  }


}
