
import { SolicitacaoItem } from './../shared/solicitacaoItem.model';
import { Solicitacao } from './../shared/solicitacao.model';
import { SolicitacaoService } from './../shared/solicitacao.service';
import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceForm } from 'src/app/shared/base-resource-form/base-resource-form.abstract';





@Component({
  selector: 'app-solicitacao-form',
  templateUrl: './solicitacao-form.component.html',
  styleUrls: ['./solicitacao-form.component.css']
})
export class SolicitacaoFormComponent extends BaseResourceForm<Solicitacao> {

  constructor(

    protected solicitacaoService: SolicitacaoService,
    protected injector: Injector) {
    super(injector, new Solicitacao(), solicitacaoService, Solicitacao.fromJson);
    this.resource.solicitacoesItens = new Array<SolicitacaoItem>();
    this.tipoItem;
    this.statusItem;



  }

  compare = 'id';
  display = false;
  solicitacaoItem: SolicitacaoItem;
  tipoItem = [
    {label: 'Serviço', value: 'S'}, {label: 'Produto', value: 'P'}
  ];
  statusItem = [{label: 'Pendente', value: 'Pendente'},
               {label: 'Cancelado', value: 'Cancelado'},
               {label: 'Aguardando', value: 'Aguardando'}];
item: string;

  ptBR = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
    monthNames: [
      'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
      'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
    ],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    today: 'Hoje',
    clear: 'Limpar'
  };

 compareFn = this._compareFn.bind(this);

  protected buildResourceForm() {
   this.resourceForm = this.formBuilder.group({
      id: [null],
      numeroSolicitacao: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(150) ]],
      dataSolicitacao: [null, Validators.required],
      dataPrazo: [null],
      solicitacoesItens: [],
      descricao: [],
      observacao: [],
      statusItem: [],
      tipoItem: [],
      qtd: [],


    });
  }

 _compareFn(a, b) {
   if (a && b ) {
     return a[this.compare] === b[this.compare];
   }
 }


  protected creationPageTitle(): string {

    return 'Cadastro de Solicitaçao';
  }

  protected editionPageTitle(): string {
     return 'Editando: ';
  }
  add(descricaoItem: string, tipo: any, status: any, qtdItem: string, obsItem: string) {


    this.solicitacaoItem = new SolicitacaoItem();
    this.solicitacaoItem.id = null;
    this.solicitacaoItem.descricao  = descricaoItem;
    this.solicitacaoItem.tipoItem   = tipo;
    this.solicitacaoItem.statusItem = status;
    this.solicitacaoItem.qtd        = qtdItem;
    this.solicitacaoItem.observacao = obsItem;
    this.resource.solicitacoesItens.push(this.solicitacaoItem);
    this.solicitacaoService.addSolicitacaoItem(this.solicitacaoItem);
    this.clearSolicitacaoItem;


  }
  delete(i: number) {

    const mustDelete = confirm('Deseja realmente excluir este item?');
    if (mustDelete) {
      this.resource.solicitacoesItens.splice(i, 1);
      this.solicitacaoService.deleteSolicitacaoItem(i);
    }

  }
  editar(solicitacaoItem: SolicitacaoItem) {





  }
  clearSolicitacaoItem() {

    this.resourceForm.get('descricao').setValue('');
    this.resourceForm.get('observacao').setValue('');
    this.resourceForm.get('statusItem').setValue('');
    this.resourceForm.get('tipoItem').setValue('');
    this.resourceForm.get('qtd').setValue('');


  }
  insertEdit(solicitacaoItem: SolicitacaoItem, edit: boolean) {

    this.display = true;

    if (edit) {
        this.resourceForm.get('descricao').setValue(solicitacaoItem.descricao);
        this.resourceForm.get('observacao').setValue(solicitacaoItem.observacao);
        this.resourceForm.get('statusItem').setValue(solicitacaoItem.statusItem);
        this.resourceForm.get('tipoItem').setValue(solicitacaoItem.tipoItem);
        this.resourceForm.get('qtd').setValue(solicitacaoItem.qtd);

      }

   }

}

