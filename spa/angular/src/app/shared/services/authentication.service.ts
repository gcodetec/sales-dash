import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { environment } from 'src/environments/environment';
import * as JwtDecode from 'jwt-decode';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {

        if (this.currentUserSubject.value != null && this.currentUserSubject.value.role == null) {
            const tokenPayload: User = JwtDecode(this.currentUserSubject.value.access_token);
            if (tokenPayload.role != null) {
                this.currentUserSubject.value.role = tokenPayload.role;
            }
        }
        if (this.currentUserSubject == null || this.currentUserSubject.value == null ) {
            return null;
        }
        // id do usuario logado
        this.currentUserSubject.value.id = +JwtDecode(this.currentUserSubject.value.access_token)['nameid'];
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
      //  return this.http.post<any>(`http://localhost:9092/oauth/token?grant_type=password&username=admin&password=123456`, null)
        return this.http.post<any>(`${environment.apiAuthentication}sigmin`, { username, password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                debugger;
                if (user && user.access_token) {
                 debugger;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
