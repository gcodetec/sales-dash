import { BaseResourceModel } from './base-resource.model';
import { Role } from './role';

export class User extends BaseResourceModel {
  login: string;
  password: string;
  firstName: string;
  token_type: string;
  active: boolean;
  username: string;
  email: string;
  access_token?: string;
  idRole: number;
  createDate: Date;
  updateDate: Date;
  userRole: Role;
  role: string[] = [];
  isMaster = false;
}
