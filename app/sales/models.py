from django.db import models
from django.contrib.auth.models import User


class Address(models.Model):
    postcode = models.CharField(max_length=100, blank=True, null=True)
    street = models.CharField(max_length=500, blank=True, null=True)
    number = models.CharField(max_length=10, blank=True, null=True)
    district = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    country = models.CharField(max_length=2)


class Company(models.Model):
    name = models.CharField(max_length=200)
    foundation_date = models.DateField(blank=True, null=True)
    document = models.CharField(max_length=14)
    user = models.OneToOneField(User, related_name='company', on_delete=models.PROTECT)


class Store(models.Model):
    company = models.ForeignKey(Company, related_name='stores', on_delete=models.PROTECT)
    external_code = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    address = models.OneToOneField(Address, on_delete=models.PROTECT, blank=True, null=True)

    @classmethod
    def find_by_external_code(cls, external_code):
        return cls.objects.filter(external_code=external_code).first()


class Customer(models.Model):
    PHYSICAL_PERSON = 'F'
    LEGAL_PERSON = 'J'

    TYPES = (
        (PHYSICAL_PERSON, 'Pessoa Física'),
        (LEGAL_PERSON, 'Pessoa Jurídica')
    )

    company = models.ForeignKey(Company, related_name='customers', on_delete=models.PROTECT)
    name = models.CharField(max_length=500)
    document = models.CharField(max_length=14)
    customer_type = models.CharField(max_length=1, choices=TYPES, default=PHYSICAL_PERSON)
    external_code = models.CharField(max_length=20)
    address = models.OneToOneField(Address, blank=True, null=True, on_delete=models.PROTECT)

    @classmethod
    def find_by_external_code(cls, external_code):
        return cls.objects.filter(external_code=external_code).first()

class Salesman(models.Model):
    company = models.ForeignKey(Company, related_name='salesmen', on_delete=models.PROTECT)
    name = models.CharField(max_length=100)


class Product(models.Model):
    company = models.ForeignKey(Company, related_name='products', on_delete=models.PROTECT)
    store = models.ForeignKey(Store, related_name='products', on_delete=models.PROTECT)
    external_code = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    unit = models.CharField(max_length=10, default='UND')
    barcode = models.CharField(max_length=50, blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=4)

    @classmethod
    def find_by_external_code(cls, external_code):
        return cls.objects.filter(external_code=external_code).first()


class Sale(models.Model):
    company = models.ForeignKey(Company, related_name='sales', on_delete=models.PROTECT)
    store = models.ForeignKey(Store, related_name='sales', on_delete=models.PROTECT)
    external_code = models.CharField(max_length=20)
    customer_id = models.ForeignKey(Customer, related_name='sales', on_delete=models.PROTECT, blank=True, null=True)
    gross_amount = models.DecimalField(max_digits=10, decimal_places=2)
    discount = models.DecimalField(max_digits=10, decimal_places=2)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date_time = models.DateTimeField()
    created_at = models.DateField(auto_now_add=True)


class SaleItem(models.Model):
    sale = models.ForeignKey(Sale, related_name='items', on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    salesman = models.ForeignKey(Salesman, blank=True, null=True, on_delete=models.PROTECT)
    name = models.CharField(max_length=200)
    unit_value = models.DecimalField(max_digits=10, decimal_places=4)
    quantity = models.DecimalField(max_digits=10, decimal_places=4)
    gross_amount = models.DecimalField(max_digits=10, decimal_places=2)
    discount = models.DecimalField(max_digits=10, decimal_places=2)
    amount = models.DecimalField(max_digits=10, decimal_places=2)




