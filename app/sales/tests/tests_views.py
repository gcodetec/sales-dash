from sales.tests import SalesDashboardTestCase

class StoreViewTest(SalesDashboardTestCase):
    def testCreateCompleteStore(self):
        c = self.autenticate()
        data = {
            'name': 'Store 1',
            'address': {
                'postcode': '41710020',
                'street': 'Rua Lavínia Magalhães',
                'number': '619',
                'district': 'Boca do Rio',
                'city': 'Salvador',
                'state': 'Bahia',
                'country': 'BR',
            }
        }

        response = c.post('/api/v1/stores', data=data, content_type='application/json')
        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])

    def testCreateStoreWithoutAddress(self):
        c = self.autenticate()
        data = {
            'name': 'Store 2'
        }

        response = c.post('/api/v1/stores', data=data, content_type='application/json')
        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])

    def testCreateStoreWithEmptyAddress(self):
        c = self.autenticate()
        data = {
            'name': 'Store 2',
            'address': {}
        }

        response = c.post('/api/v1/stores', data=data, content_type='application/json')
        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])

class CustomerViewTest(SalesDashboardTestCase):
    def testCreateCompleteCustomer(self):
        c = self.autenticate()
        data = {
            'name': 'João da Silva',
            'document':'01623735548',
            'type': 'F',
            'external_code': '100',
            'address': {
                'postcode': '41710020',
                'street': 'Rua Lavínia Magalhães',
                'number': '619',
                'district': 'Boca do Rio',
                'city': 'Salvador',
                'state': 'Bahia',
                'country': 'BR',
            }
        }

        response = c.post('/api/v1/customers', data=data, content_type='application/json')

        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])

    def testCreateCustomerWithoutAddress(self):
        c = self.autenticate()
        data = {
            'name': 'João da Silva',
            'document':'01623735548',
            'type': 'F',
            'external_code': '110'
        }

        response = c.post('/api/v1/customers', data=data, content_type='application/json')

        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])

class ProductViewTest(SalesDashboardTestCase):
    def testCreateProduct(self):
        c = self.autenticate()
        data = {
            'external_code': '100',
            'name': 'Red T-Shirt',
            'unit': 'PC',
            'barcode': '09866788',
            'price': 10.5,
            'store_code': self.store1.external_code
        }

        response = c.post('/api/v1/products', data=data, content_type='application/json')

        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])

class SaleViewTest(SalesDashboardTestCase):
    def testCreateSale(self):
        c = self.autenticate()
        data = {
            'store_code': self.store1.external_code,
            # 'customer_code': customer.external_code,
            'external_code': '100',
            'gross_amount': 250.0,
            'discount': 10.0,
            'amount': 240.0,
            'date_time': '2020-11-10 12:45:10',
            'items': [
                {
                    'product_code': '200',
                    'name': 'Green Cap',
                    'unit_value': 25.0,
                    'quantity': 10,
                    'gross_amount': 250.0,
                    'discount': 10.0,
                    'amount': 240.0,
                }
            ]
        }

        response = c.post('/api/v1/sales', data=data, content_type='application/json')

        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])

    def testCreateSaleWithCustomer(self):
        c = self.autenticate()
        data = {
            'store_code': self.store1.external_code,
            'customer': {
                'name': 'Abigail Moraes',
                'external_code': '250'
            },
            'external_code': '100',
            'gross_amount': 250.0,
            'discount': 10.0,
            'amount': 240.0,
            'date_time': '2020-11-10 12:45:10',
            'items': [
                {
                    'product_code': '200',
                    'name': 'Green Cap',
                    'unit_value': 25.0,
                    'quantity': 10,
                    'gross_amount': 250.0,
                    'discount': 10.0,
                    'amount': 240.0,
                }
            ]
        }

        response = c.post('/api/v1/sales', data=data, content_type='application/json')

        json_response = response.json()

        self.assertEquals(200, response.status_code)
        self.assertEquals('OK', json_response['status'])
