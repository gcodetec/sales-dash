from django.test import TestCase
from django.contrib.auth.models import User
from django.test import Client

from sales.models import Company
from sales.services import StoreService

class SalesDashboardTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user1 = User.objects.create_user('company1@gmail.com', 'company1@gmail.com', 'secret')
        cls.company1 = Company.objects.create(name='Company One Co.', document='22304267000105', user=cls.user1)

        service = StoreService()
        data = {
            'company':cls.company1,
            'name':'Store 1',
            'external_code': '22304267000101'
        }
        cls.store1 = service.create(data)

    def autenticate(self):
        c = Client()
        c.login(username='company1@gmail.com', password='secret')
        return c
