from django.utils import timezone

from sales.tests import SalesDashboardTestCase
from sales.services import CustomerService, ProductService, SaleService

class CustomerServiceTest(SalesDashboardTestCase):
    def testBlockCreateDuplicateCustomer(self):
        service = CustomerService()
        data = {
            'name': 'João da Silva',
            'document':'01623735548',
            'type': 'F',
            'company': self.company1,
            'external_code': '100',
        }

        customer = service.create(data)

        self.assertEquals('01623735548', customer.document)

        data = {
            'name': 'João da Silva',
            'document':'01623735548',
            'type': 'F',
            'company': self.company1,
            'external_code': '100',
        }

        customer2 = service.create(data)

        self.assertEquals('01623735548', customer2.document)
        self.assertEquals('100', customer.external_code)
        self.assertEquals(customer2.id, customer.id)

class ProductServiceTest(SalesDashboardTestCase):
    def testCreateProduct(self):
        service = ProductService()
        data = {
            'external_code': '1',
            'name': 'Product 1',
            'unit': 'PC',
            'price': 10.5,
            'barcode': '',
            'store': self.store1,
            'company': self.company1
        }

        product = service.create(data)

        self.assertEquals('1', product.external_code)
        self.assertEquals('PC', product.unit)

    def testNotCreateDuplicateProduct(self):
        service = ProductService()
        data = {
            'external_code': '2',
            'name': 'Product 2',
            'unit': 'PC',
            'price': 11.5,
            'barcode': '',
            'store': self.store1,
            'company': self.company1
        }

        product = service.create(data)

        self.assertEquals('2', product.external_code)
        self.assertEquals('PC', product.unit)
        self.assertEquals('Product 2', product.name)

        data = {
            'external_code': '2',
            'name': 'Product 22',
            'unit': 'UND',
            'price': 11.5,
            'barcode': '',
            'store': self.store1,
            'company': self.company1
        }

        product = service.create(data)

        self.assertEquals('2', product.external_code)
        self.assertEquals('PC', product.unit)
        self.assertEquals('Product 2', product.name)

class SaleServiceTest(SalesDashboardTestCase):
    def testCreateSale(self):
        service = SaleService()
        customer_service = CustomerService()
        product_service = ProductService()

        customer_data = {
            'name': 'João da Silva',
            'document':'01623735548',
            'type': 'F',
            'company': self.company1,
            'external_code': '100',
        }
        customer = customer_service.create(customer_data)

        product_data = {
            'external_code': '1',
            'name': 'Product 1',
            'unit': 'PC',
            'price': 25.0,
            'barcode': '123456789',
            'store': self.store1,
            'company': self.company1
        }
        product = product_service.create(product_data)


        data = {
            'company': self.company1,
            'store_code': self.store1.external_code,
            'customer_code': customer.external_code,
            'external_code': '100',
            'gross_amount': 250.0,
            'discount': 10.0,
            'amount': 240.0,
            'date_time': timezone.now(),
            'items': [
                {
                    'product_code': product.external_code,
                    'name': product.name,
                    'unit_value': 25.0,
                    'quantity': 10,
                    'gross_amount': 250.0,
                    'discount': 10.0,
                    'amount': 240.0,
                }
            ]
        }

        sale = service.create(data)

        self.assertEquals(1, sale.items.count())
        self.assertEquals('100', sale.external_code)
