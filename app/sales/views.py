import json

from django.shortcuts import render
from django.db import DatabaseError
from django.views import View
from django.http import JsonResponse
from .services import ProductService, SaleService, StoreService, CustomerService

class DataPersistView(View):
    def post(self, request):
        # try:
        payload = json.loads(request.body)

        self.process_request(request=request, payload=payload)

        data = {'status': 'OK'}
        # except DatabaseError as err:
        #     data = {'status':'Error', 'reason': '{} - {}'.format('DatabaseError', str(err))}
        # except Exception as ex:
        #     data = {'status':'Error', 'reason': '{} - {}'.format('Exception', ex)}

        return JsonResponse(data)

    def process_request(self, request, payload: dict):
        raise Exception('Method not implemented')

class StoreView(DataPersistView):
    def process_request(self, request, payload):
        service = StoreService()
        payload['company'] = request.user.company
        return service.create(payload)

class CustomerView(DataPersistView):
    def process_request(self, request, payload):
        service = CustomerService()
        payload['company'] = request.user.company
        return service.create(payload)

class ProductView(DataPersistView):
    def process_request(self, request, payload):
        service = ProductService()
        payload['company'] = request.user.company
        return service.create(payload)

class SalesView(DataPersistView):
    def process_request(self, request, payload):
        service = SaleService()
        payload['company'] = request.user.company
        return service.create(payload)
