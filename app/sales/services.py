from django.db import transaction
from .models import Company, Product, Store, Address, Customer, Sale, SaleItem

class StoreService(object):
    def create(self, data) -> Store:
        with transaction.atomic():
            store = Store()
            store.name = data['name']
            store.company = data['company']

            if 'address' in data:
                address = Address(**data['address'])
                if address.save():
                    store.address = address

            store.save()

            return store

class CustomerService(object):
    def create(self, data: dict) -> Customer:

        customer = Customer.objects.filter(company=data['company'], external_code=data['external_code']).first()

        if customer is not None:
            return customer

        with  transaction.atomic():
            customer = Customer()
            customer.company = data['company']
            customer.name = data['name']

            if 'document' in data:
                customer.document = data['document']

            if 'type' in data:
                customer.customer_type = data['type']

            customer.external_code = data['external_code']

            if 'address' in data:
                address = Address(**data['address'])
                if address.save():
                    customer.address = address

            customer.save()

            return customer


class ProductService(object):
    def create(self, data: dict) -> Product:
        product = Product.objects.filter(company=data['company'], external_code=data['external_code']).first()

        if product is not None:
            return product

        if 'store_code' in data:
            data['store'] = Store.find_by_external_code(data['store_code'])

        product = Product()

        product.company = data['company']
        product.store = data['store']
        product.external_code = data['external_code']
        product.name = data['name']
        product.unit = data['unit']

        if 'barcode' in data:
            product.barcode = data['barcode']
        product.price = data['price']

        product.save()

        return product

class SaleService(object):
    def create(self, data: dict) -> Sale:
        with transaction.atomic():
            sale = Sale()
            customer = None

            if 'customer_code' in data:
                customer = Customer.find_by_external_code(data['customer_code'])

            if 'customer' in data:
                if isinstance(data['customer'], dict):
                    customer_service = CustomerService()
                    data['customer']['company'] = data['company']
                    customer = customer_service.create(data['customer'])
                elif isinstance(data['customer'], Customer):
                    customer = data['customer']

            store = Store.find_by_external_code(data['store_code'])

            sale.company = data['company']
            sale.store = store

            if customer is not None:
                sale.customer = customer

            sale.external_code = data['external_code']
            sale.gross_amount = data['gross_amount']
            sale.discount = data['discount']
            sale.amount = data['amount']
            sale.date_time = data['date_time']

            sale.save()

            for item in data['items']:
                self.create_item(sale, item)

            return sale

    def create_item(self, sale: Sale, item: dict) -> SaleItem:
        sale_item = SaleItem()

        product = Product.find_by_external_code(item['product_code'])

        if product is None:
            service = ProductService()
            product_data = {
                'name': item['name'],
                'external_code': item['product_code'],
                'company': sale.company,
                'store': sale.store,
                'unit': 'UND',
                'price': item['unit_value']
            }

            if 'barcode' in item:
                product_data['barcode'] = item['barcode']
                sale_item = item['barcode']

            product = service.create(product_data)

        sale_item.sale = sale
        sale_item.product = product
        # sale_item.salesman
        sale_item.name = item['name']
        sale_item.unit_value = item['unit_value']
        sale_item.quantity = item['quantity']
        sale_item.gross_amount = item['gross_amount']
        sale_item.discount = item['discount']
        sale_item.amount = item['amount']

        sale_item.save()

        return sale_item
