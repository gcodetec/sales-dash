package com.salesdash.salesdashapi.repositories;

import java.util.List;

import com.salesdash.salesdashapi.models.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findLastName(String lastName);
    User findById(long id);
}
