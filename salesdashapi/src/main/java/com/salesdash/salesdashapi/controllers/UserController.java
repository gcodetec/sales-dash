package com.salesdash.salesdashapi.controllers;

import java.util.List;

import com.salesdash.salesdashapi.models.User;
import com.salesdash.salesdashapi.repositories.UserRepository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

	@GetMapping("/")
	public Iterable<User> index(UserRepository r) {
		return r.findAll();
	}

	@PostMapping("/")
	public User create(UserRepository r) {
		User user = new User("gwmoura@gmail.com", "superpass");
		user = r.save(user);
		return user;
	}

}
