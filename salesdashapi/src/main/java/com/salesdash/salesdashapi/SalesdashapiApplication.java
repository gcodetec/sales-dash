package com.salesdash.salesdashapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, MongoAutoConfiguration.class })
@RestController
public class SalesdashapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesdashapiApplication.class, args);
	}

	@GetMapping("/")
	public String index() {
		return String.format("Welcome to SalesDash!");
	}

	@GetMapping("/healthcheck")
	public String healthcheck() {
		return String.format("ok");
	}

}
