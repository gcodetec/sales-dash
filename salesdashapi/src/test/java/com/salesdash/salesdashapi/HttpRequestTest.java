package com.salesdash.salesdashapi;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	private String request(String path) {
		String baseUrl = "http://localhost:" + port;
		return this.restTemplate.getForObject(baseUrl + path, String.class);
	}

	@Test
	public void indexShouldReturnDefaultMessage() throws Exception {
		assertThat(this.request("/")).contains("Welcome to SalesDash!");
	}

	@Test
	public void healthcheckShouldReturnOk() throws Exception {
		assertThat(this.request("/healthcheck")).contains("ok");
	}
}
