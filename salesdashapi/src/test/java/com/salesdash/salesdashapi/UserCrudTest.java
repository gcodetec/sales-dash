package com.salesdash.salesdashapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.salesdash.salesdashapi.models.User;
import com.salesdash.salesdashapi.repositories.UserRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserCrudTest {
    @Autowired
    private UserRepository repository;

    @Test
	void contextLoads() {
	}

    @Test
    void createUser() {
        User u = new User("email@test.com", "newpasssword");
        u = this.repository.save(u);
        assertNotNull(u);
        assertEquals("email@test.com", u.getEmail());
    }
}
