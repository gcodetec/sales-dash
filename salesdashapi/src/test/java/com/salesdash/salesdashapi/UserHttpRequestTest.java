package com.salesdash.salesdashapi;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import com.salesdash.salesdashapi.models.User;
import com.salesdash.salesdashapi.repositories.UserRepository;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
@DataJpaTest
public class UserHttpRequestTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private UserRepository repository;

	@Autowired
    private MockMvc mvc;

	private String request(String method, String path) {
		String baseUrl = "http://localhost:" + port + "/api/v1/users";
		return this.restTemplate.getForObject(baseUrl + path, String.class);
	}

	@Test
	public void indexShouldReturnListOfusers() throws Exception {
		assertThat(this.request("get", "/")).contains("[]");
	}

	@Test
	public void createShouldReturnUser() throws Exception {
		String baseUrl = "http://localhost:" + port + "/api/v1/users";
		User user = new User("test@email.com", "superpass");
		HttpEntity<User> request = new HttpEntity<>(user);
		ResponseEntity<String> response = this.restTemplate.postForEntity(baseUrl + "/", request, String.class);
		assertThat(response.getBody()).contains("ok");
		assertEquals(201, response.getStatusCode());
	}
}
